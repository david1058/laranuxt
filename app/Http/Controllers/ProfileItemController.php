<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProfileItemResource;
use App\Http\Resources\ProfileResource;
use App\Models\ProfileItem;
use Illuminate\Http\Request;

class ProfileItemController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'start_date' => 'required',
            'category' => 'required',
            'profile_id' => 'required',
        ];
        $this->query['options'] = $rules;
        $this->verify();

        $profileItem = new ProfileItem($request->all());
        $profileItem->save();
        return $this->render(new ProfileResource($profileItem->profile->load('profileitems')));
//        return $this->render(new ProfileItemResource($profileItem));
    }
}
