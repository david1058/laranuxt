<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'bio' => $this->bio,
            'user' => new UserResource($this->user),
            'profileitems' => ProfileItemResource::collection($this->whenLoaded('profileitems')),
            'last_updated' => $this->updated_at->tz('America/Chicago')->format('M d, Y'),
        ];
    }
}
