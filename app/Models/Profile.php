<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'title', 'bio',
    ];

    protected $rules = [
        'user_id' => 'required|integer|exists:users',
        'title' => 'required|string',
        'bio' => 'required|string',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function profileItems()
    {
        return $this->hasMany(ProfileItem::class);
    }
}
