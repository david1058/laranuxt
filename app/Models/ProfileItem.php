<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfileItem extends Model
{
    use HasFactory;

    const CATEGORY_CHOICES = [
        'experience',
        'education',
        'hobby',
    ];

    protected $fillable = [
        'title', 'start_date', 'end_date', 'description', 'category', 'profile_id'
    ];

    protected $casts = [
        'start_date' => 'date',
        'end_date' => 'date',
    ];

    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }
}
