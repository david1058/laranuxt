export const state = () => ({
  profile: {},
  user: {},
})

export const mutations = {
  setUserObject(state, payload) {
    state.user = payload
  },
  setProfileObject(state, payload) {
    state.profile = payload
  },
}
