<?php

namespace Database\Factories;

use App\Models\Profile;
use App\Models\ProfileItem;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProfileItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProfileItem::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->jobTitle,
            'start_date' => $this->faker->date,
            'end_date' => $this->faker->date,
            'description' => $this->faker->paragraph,
            'category' => ProfileItem::CATEGORY_CHOICES[rand(0, count(ProfileItem::CATEGORY_CHOICES))],
            'profile_id' => Profile::factory(),
        ];
    }
}
