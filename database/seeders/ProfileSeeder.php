<?php

namespace Database\Seeders;

use App\Models\Profile;
use App\Models\ProfileItem;
use App\Models\User;
use Illuminate\Database\Seeder;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Profile::factory([
            'title' => 'Full Stack Developer',
            'bio' => 'Hello, my name is David Gitonga. I have 10 years experience as a software developer. I currently use Laravel, Vue, PHP, JavaScript, CSS & Laravel Livewire on most of my current projects.',
        ])->for(
            User::factory([
                'first_name' => 'David',
                'last_name' => 'Gitonga',
                'email' => 'david@dgitts.com',
                'email_verified_at' => now(),
            ])->create())
            ->create();
    }
}
