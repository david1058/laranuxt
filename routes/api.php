<?php

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', [Controller::class, 'routes'])
    ->name('route information')
    ->withoutMiddleware('api');
Route::get('/example', [Controller::class, 'example'])->name('example route');
Route::get('/error', [Controller::class, 'error'])->name('error route');

Route::resource('profiles', \App\Http\Controllers\ProfileController::class, ['except' => ['create', 'edit']]);
Route::resource('users', \App\Http\Controllers\UserController::class, ['only' => ['store', 'show']]);
Route::resource('profile-items', \App\Http\Controllers\ProfileItemController::class, ['only' => ['store']]);
