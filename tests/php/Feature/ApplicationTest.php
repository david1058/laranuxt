<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;


class ApplicationTest extends TestCase
{
    /** @test */
    public function applicationEntryEndpointRuns()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }
}
