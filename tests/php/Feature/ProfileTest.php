<?php

namespace Tests\Feature;

use App\Models\Profile;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ProfileTest extends TestCase
{
    /** @test */
    public function indexEndpoint()
    {
        Profile::factory()->create();
        $this->json('GET', "profiles")
                ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'title',
                        'bio',
                        'user' => [
                            'first_name',
                            'last_name',
                            'full_name',
                            'email',
                            'avatar',
                        ],
                        'last_updated',
                    ],
                ],
            ]);
    }

    /** @test */
    public function storeEndpoint()
    {
        $profile = Profile::factory()
            ->for(User::factory()->create())
            ->make();
        $payload = [
            'user_id' => $profile->user_id,
            'title' => $profile->title,
            'bio' => $profile->bio,
        ];
        $this->json('POST', "profiles", $payload)
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'status',
                'benchmark',
                'query' => [
                    'options',
                    'params',
                ],
                'data' => [
                    'id',
                    'title',
                    'bio',
                    'user' => [
                        'first_name',
                        'last_name',
                        'full_name',
                        'email',
                        'avatar',
                    ],
                    'last_updated',
                ],
            ]);
    }

    /** @test */
    public function showEndpoint()
    {
        $profile = Profile::factory()->create();
        $this->json('GET', "profiles/{$profile->id}")
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'status',
                'benchmark',
                'query' => [
                    'options',
                    'params',
                ],
                'data' => [
                    'id',
                    'title',
                    'bio',
                    'user' => [
                        'first_name',
                        'last_name',
                        'full_name',
                        'email',
                        'avatar',
                    ],
                    'last_updated',
                ],
            ]);
    }

    /** @test */
    public function updateEndpoint()
    {
        $profile = Profile::factory()
            ->for(User::factory()->create())
            ->create();
        $payload = [
            'title' => 'updated title text',
        ];
        $this->json('PUT', "profiles/{$profile->id}", $payload)
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'status',
                'benchmark',
                'query' => [
                    'options',
                    'params',
                ],
                'data' => [
                    'id',
                    'title',
                    'bio',
                    'user' => [
                        'first_name',
                        'last_name',
                        'full_name',
                        'email',
                        'avatar',
                    ],
                    'last_updated',
                ],
            ]);
        $this->assertEquals($payload['title'], $profile->fresh()->title);
    }

    /** @test */
    public function deleteEndpoint()
    {
        $profile = Profile::factory()->create();
        $this->json('DELETE', "profiles/{$profile->id}")
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                "success",
                "message",
            ]);
    }
}
